# $1: User name



USER_NAME=$1
pass=$(getent passwd | grep ^$USER_NAME:\\*)

USER_ID=$( echo $pass | awk -F: '{print $3}' )
#Default is speech group
GROUP_ID=2520 #$( echo $pass | awk -F: '{print $4}' )
echo "Creating Docker file for User "$1" with USER ID "$USER_ID" and GUID (speech) "$GROUP_ID
sed s/%%USER%%/$USER_NAME/g Dockerfile_Kaldi_USER | sed s/%%GUID%%/$GROUP_ID/g | sed s/%%UID%%/$USER_ID/g | sed s/%%GNAME%%/speech/g  > Dockerfile_Kaldi_$USER_NAME

